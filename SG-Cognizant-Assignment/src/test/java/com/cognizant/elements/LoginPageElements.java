package com.cognizant.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPageElements {
	WebDriver driver;
	
	public LoginPageElements(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.NAME, using = "CPUID") 
	 public WebElement txtbx_Nric;
	
	@FindBy(how = How.NAME, using = "CPUID_FullName") 
	 public WebElement txtbx_Name;

	@FindBy(how = How.NAME, using = "CPEntID") 
	 public WebElement txtbx_Uen;
	
	@FindBy(how = How.NAME, using = "CPRole") 
	 public WebElement drpDown_Role;
	
	@FindBy(how = How.XPATH, using = "//select[@name='CPRole']//following::button") 
	 public WebElement btn_Login;

}
